#!/usr/bin/env perl

use strict;
use warnings;
use List::Util 'sum';

my (%letters, %order) = () x 2;
my (@numbers, @unsortedNumbers) = () x 2;
my ($resource, $resources, $pre, $sum) = (0) x 4;

my ($c, $h) = (0) x 2;

my @nimilista = map { chomp; $_ } <>;

while(@nimilista) {
    $resource = shift @nimilista;
    
    foreach(@nimilista) {
        $resources = "$resource $_";

        %letters = (
            'p' => 0,
            'a' => 0,
            'i' => 0,
            'r' => 0,
            's' => 0
        );
        
        map { $letters{lc("$_")}++ } $resources =~ m/([pairs])/ig;

        @numbers = map { $letters{$_} } sort pairs ( keys %letters );

        while((keys @numbers) > 2) {
            $pre = shift @numbers;
            @numbers = map { 
                $sum = $_ + $pre; 
                $pre = $_; 
                $sum < 10 ? $sum : sum ( $sum =~ m/./g )
            } @numbers;
        }
        $sum = join('', @numbers);
        
        $c++;
        if($sum == 99) {
            $h++;
            print "Hurray, I found a perfect match!!!\n";
        }
    }
}

print "\n\nFound $h / $c perfect matches\n\n";

sub pairs {
    %order = (
        'p' => 1,
        'a' => 2,
        'i' => 3,
        'r' => 4,
        's' => 5
    );

    $order{"$a"} <=> $order{"$b"};
}
