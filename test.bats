#!/usr/bin/env bats

@test "Matti Meikäläinen Erkki Esimerkki" {
  result="$(./pairs.pl < fixtures/MattijaErkki.txt)"
  [[ "$result" =~ '0 / 1' ]]
}

@test "Erkki Esimerkki Tiina Testaaja" {
  result="$(./pairs.pl < fixtures/ErkkijaTiina.txt)"
  [[ "$result" =~ '0 / 1' ]]
}

@test "Piatta Kuoppamäki" {
  result="$(./pairs.pl < fixtures/PauliinajaJeremy.txt)"
  [[ "$result" =~ '1 / 1' ]]
}
